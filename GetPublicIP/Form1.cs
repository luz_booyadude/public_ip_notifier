﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using Ini;
using System.IO;
using System.Web;
using System.Net.Mail;
using System.Net;

namespace GetPublicIP
{
    public partial class Form1 : Form
    {
        public string smtpAddress;
        public int portNumber;
        public bool enableSSL;
        public string emailFrom;
        public string password;
        public string emailTo;
        public string subject;

        public Form1()
        {
            InitializeComponent();
            LoadINI();
            IpScan = new Thread(IPWorker_DoWork);
            IpScan.IsBackground = true;
            IpScan.Start();
        }

        private Thread IpScan
        {
            get;
            set;
        }

        private void IPWorker_DoWork()
        {
            // The sender is the BackgroundWorker object we need it to
            // report progress and check for cancellation.
            //NOTE : Never play with the UI thread here...
            TimeSpan t = DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Unspecified));
            int epoch = (int)t.TotalSeconds + 28800;
            DateTime epoch1 = new DateTime(1970, 1, 1);
            DateTime myDateTime = epoch1.AddSeconds(epoch);
            string time = Convert.ToString(myDateTime);
            string IP = GetPublicIP();
            UpdateMessage(IP, time);
            SendEmail();
            while (true)
            {
                string IPScan = GetPublicIP();
                if (!(IP.Equals(IPScan)))
                {
                    t = DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Unspecified));
                    epoch = (int)t.TotalSeconds + 28800;
                    myDateTime = epoch1.AddSeconds(epoch);
                    SendEmail();
                    IP = IPScan;
                    time = Convert.ToString(myDateTime);
                    UpdateMessage(IP, time);
                }
                else
                {
                    //notifyIcon1.BalloonTipText(IP + "\r\nLast Update: " + Convert.ToString(myDateTime));  //Console.WriteLine("IP not changed.");
                }
                Thread.Sleep(600000);
            }

        }

        void UpdateMessage(string IP, string updatetime)
        {
            Action action = () => textBox1.Text = IP;
            this.Invoke(action);
            Action action2 = () => textBox2.Text = updatetime;
            this.Invoke(action2);
        }

        public void LoadINI()
        {
            string path = Directory.GetCurrentDirectory().Replace(@"\", @"\\") + "\\client.ini";
            IniFile ini = new IniFile(path);
            smtpAddress = ini.IniReadValue("Setting", "Server");
            portNumber = Convert.ToInt32(ini.IniReadValue("Setting", "Port"));
            enableSSL = Convert.ToBoolean(ini.IniReadValue("Setting", "EnableSSL"));
            emailFrom = ini.IniReadValue("Setting", "EmailFrom");
            password = ini.IniReadValue("Setting", "Password");
            emailTo = ini.IniReadValue("Setting", "EmailTo");
            subject = ini.IniReadValue("Setting", "Subject");
        }

        public void SendEmail()
        {
            string body = GetPublicIP();

            using (MailMessage mail = new MailMessage())
            {
                mail.From = new MailAddress(emailFrom);
                mail.To.Add(emailTo);
                mail.Subject = subject;
                mail.Body = body;
                mail.IsBodyHtml = false;
                // Can set to false, if you are sending pure text.

                //mail.Attachments.Add(new Attachment("C:\\SomeFile.txt"));
                //mail.Attachments.Add(new Attachment("C:\\SomeZip.zip"));

                using (SmtpClient smtp = new SmtpClient(smtpAddress, portNumber))
                {
                    smtp.Credentials = new NetworkCredential(emailFrom, password);
                    smtp.EnableSsl = enableSSL;
                    smtp.Send(mail);
                }
            }
        }

        public static string GetPublicIP()
        {
            string url = "http://checkip.dyndns.org";
            WebRequest req = WebRequest.Create(url);
            using(WebResponse resp = req.GetResponse())
            {
                StreamReader sr = new StreamReader(resp.GetResponseStream());
                string response = sr.ReadToEnd().Trim();
                string[] a = response.Split(':');
                string a2 = a[1].Substring(1);
                string[] a3 = a2.Split('<');
                string a4 = a3[0];
                return a4;
            }
 
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                Hide();
                notifyIcon1.Visible = true;
            }
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            Show();
            this.WindowState = FormWindowState.Normal;
            notifyIcon1.Visible = false;
        }
    }
}
