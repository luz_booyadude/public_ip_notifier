# Public IP Notifier

## Prerequisite
1. Microsoft Visual Studio 2015 (https://www.visualstudio.com/products/visual-studio-community-vs)
2. Some knowledge of VS

## How to compile
1. Install VS2015
2. Open GetPublicIP.sln
3. Build the solution

## How to run the software
1. Locate client.ini file inside GetPublicIP folder.
2. Open the file, enter the required information and save.
3. Run the program. 

## How it works
1. The software will check the public IP and notify it to user's email.
2. Every 10 minutes the software will recheck and compare the IP. If it is different it will notify the user.
3. Each check the apps will suspend the thread, thus the CPU cycle will not be used.